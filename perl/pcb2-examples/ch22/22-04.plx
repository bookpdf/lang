# in TitleDumper.pm
# TitleDumper.pm -- SAX handler to display titles in books file
package TitleDumper;

use base qw(XML::SAX::Base);

my $in_title = 0;

# if we're entering a title, increase $in_title
sub start_element {
  my ($self, $data) = @_;
  if ($data->{Name} eq 'title') {
    $in_title++;
  }
}

# if we're leaving a title, decrease $in_title and print a newline
sub end_element {
  my ($self, $data) = @_;
  if ($data->{Name} eq 'title') {
    $in_title--;
    print "\n";
  }
}

# if we're in a title, print any text we get
sub characters {
  my ($self, $data) = @_;
  if ($in_title) {
    print $data->{Data};
  }
}

1;