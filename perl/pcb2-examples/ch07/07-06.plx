#!/usr/bin/perl -n
# countchunks - count how many words are used.
# skip comments, and bail on file if __END__
# or __DATA__ seen.
for (split /\W+/) {
    next LINE if /^#/;
    close ARGV if /__(DATA|END)__/;
    $chunks++;
}
END { print "Found $chunks chunks\n" }