#!/usr/bin/perl
# lowercase - turn all lines into lowercase
while (<>) {                  # loop over lines on command line
    s/(\p{Letter})/\l$1/g;    # change all letters to lowercase
    print;
}