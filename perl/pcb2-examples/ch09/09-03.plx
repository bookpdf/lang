#!/usr/bin/perl
# rmtree1 - remove whole directory trees like rm -r
use File::Find;
die "usage: $0 dir ..\n" unless @ARGV;
find {
    bydepth   => 1,
    no_chdir  => 1,
    wanted    => sub { 
        if (!-l && -d _) {
            rmdir     or warn "couldn't rmdir directory $_: $!";
        } else {
            unlink    or warn "couldn't unlink file $_: $!";
        }
    }
} => @ARGV;